# How to Prove It

> This repository has the function to put the solution of the exercises of
> "How to Prove It" by [Daniel J. Velleman](https://www.amherst.edu/people/facstaff/djvelleman)

We want to have a place of reference for all those who find difficulties in solving the exercises.

![Cover of the book](https://images-na.ssl-images-amazon.com/images/I/41-1Wu3OwyL._SX310_BO1,204,203,200_.jpg)